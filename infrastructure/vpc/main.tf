terraform {
  required_version = ">= 0.14.9"
}

data "aws_availability_zones" "available" {
}

resource "aws_vpc" "todoapp_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "todoapp_vpc"
  }
}

resource "aws_subnet" "public_subnet" {
  # count                   = var.az_count
  vpc_id                  = "${aws_vpc.todoapp_vpc.id}"
  # availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.1.0/24"
  tags = {
    Name        = "todoapp_public_subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  # count                   = var.az_count
  vpc_id                  = "${aws_vpc.todoapp_vpc.id}"
  # availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.2.0/24"
  tags = {
    Name        = "todoapp_private_subnet"
  }
}

resource "aws_internet_gateway" "todoapp_gw" {
  vpc_id = aws_vpc.todoapp_vpc.id
  tags = {
    Name        = "todoapp_gw"
  }
}

resource "aws_eip" "gw" {
  # count      = var.az_count
  vpc        = true
  depends_on = [aws_internet_gateway.todoapp_gw]
}

resource "aws_nat_gateway" "gw" {
  # count         = var.az_count
  subnet_id     = aws_subnet.public_subnet.id
  allocation_id = aws_eip.gw.id

  tags = {
    "Name" = "gw NAT"
  }

  depends_on = [aws_internet_gateway.todoapp_gw]
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.todoapp_vpc.id

  tags = {
    Name        = "todoapp-route-table"
  }
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.todoapp_gw.id
}
