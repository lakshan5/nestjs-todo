output "vpc_id" {
    value = aws_vpc.todoapp_vpc.id
}

output "subnet" {
    value = aws_subnet.private_subnet.vpc_id
}

output "pub_sub" {
    value = aws_subnet.public_subnet.vpc_id
}