variable "aws_region" {
  description = "The AWS region things are created in"
  default = "us-west-2"
}

variable "profile" {
  description = "AWS credential profile"
  default = "lakshan@surge.global"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}