module "vpc" {
  source = "./vpc"
}

module "security" {
  source  = "./security"
  vpc_id  = "${module.vpc.vpc_id}"
  pub_sub = "${module.vpc.pub_sub}"
}

module "ecs" {
  source = "./ecs"
  security_grp = "${module.security.security_grp}"
  subnet = "${module.vpc.subnet}"
  alb_target_grp = "${module.security.alb_target_grp}"
  alb_listner = "${module.security.alb_listner}"
}