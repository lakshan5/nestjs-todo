variable "aws_region" {
  description = "The AWS region things are created in"
  default = "us-west-2"
}

variable "profile" {
  description = "AWS credential profile"
  default = "lakshan@surge.global"
}


variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}

variable "health_check_path" {
  default = "/"
}

variable "vpc_id" {
  description = "vpc id"
}
variable "pub_sub" {
  description = "public subnet"
}
