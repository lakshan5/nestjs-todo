
output "alb_hostname" {
  value = aws_alb.main.dns_name
}

output "security_grp" {
  value = aws_security_group.ecs_tasks.id
}

output "alb_target_grp" {
  value = aws_alb_target_group.app.id
}

output "alb_listner" {
  value = aws_alb_listener.api
}