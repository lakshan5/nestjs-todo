terraform {
  required_version = ">= 0.14.9"
}
resource "aws_ecr_repository" "todoapp-ecr" {
  name = "todo-app-ecr"
}

resource "aws_ecs_cluster" "todoapp-ecs" {
  name = "todoapp-ecs"
}

resource "aws_ecs_task_definition" "ecs_task" {
  family                   = "ecs-task"
  container_definitions    = <<DEFINITION
  [
    {
      "name": "ecs-task",
      "image": "${aws_ecr_repository.todoapp-ecr.repository_url}",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 3000,
          "hostPort": 3000
        }
      ],
      "memory": 512,
      "cpu": 256
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # Stating that we are using ECS Fargate
  network_mode             = "awsvpc"    # Using awsvpc as our network mode as this is required for Fargate
  memory                   = 512         # Specifying the memory our container requires
  cpu                      = 256         # Specifying the CPU our container requires
  execution_role_arn       = "${aws_iam_role.ecsTaskExecutionRole.arn}"
}

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = "${aws_iam_role.ecsTaskExecutionRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

 resource "aws_ecs_service" "main" {
  name            = "todoapp-service"
  cluster         = aws_ecs_cluster.todoapp-ecs.id
  task_definition = aws_ecs_task_definition.ecs_task.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [var.security_grp]
    subnets          = [var.subnet]
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = var.alb_target_grp
    container_name   = "${aws_ecs_task_definition.ecs_task.family}"
    container_port   = 3000
  }

  depends_on = [var.alb_listner, aws_iam_role_policy_attachment.ecsTaskExecutionRole_policy]
}