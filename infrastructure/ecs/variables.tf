variable "aws_region" {
  description = "The AWS region things are created in"
  default = "us-west-2"
}

variable "profile" {
  description = "AWS credential profile"
  default = "lakshan@surge.global"
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "security_grp" {
  description = "security group"
}

variable "subnet" {
  description = "subnets"
}

variable "alb_target_grp" {
  description = "alb_target_groups"
}

variable "alb_listner" {
  description = "alb_listner"
}