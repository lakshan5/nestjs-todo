import { TodoDTO } from './todo.dto';
import { IsDateString } from 'class-validator';

export class UpdateTodoDto extends TodoDTO {

    @IsDateString()
    modifiedAt: Date;
}
