import { IsBoolean, IsNotEmpty, IsString} from 'class-validator';

export class TodoDTO {
    
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsBoolean({message: 'it must be a boolean'})
  completed: boolean;
  }
