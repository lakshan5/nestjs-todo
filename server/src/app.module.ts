import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TodoModule } from './todo/todo/todo.module';

@Module({
  imports : [
    TodoModule,
    //MongooseModule.forRoot('mongodb+srv://lakshansurge:lakshansurge@cluster0.xsdlg.mongodb.net/todo?retryWrites=true&w=majority')],
    MongooseModule.forRoot('mongodb://mongo:27017/todo')],
})
export class AppModule {}

